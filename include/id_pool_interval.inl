/*	This file is part of id_pool.
	Copyright(C) 2018 by Karl Wesley Hutchinson

	id_pool is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	id_pool is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with id_pool. If not, see <http://www.gnu.org/licenses/>.
*/

namespace detail_interval
{

template< typename T >
class interval
{
public:
	interval():
		min(),
		max()
	{ }

	interval( T const i_min, T const i_max ):
		min(i_min),
		max(i_max)
	{ }

	//-------- public member variables --------
	T min;
	T max;
};

template< typename T >
bool operator<( interval<T> const& lhs, T const rhs )
{
	return lhs.max < rhs;
}

template< typename T >
bool operator>( interval<T> const& lhs, T const rhs )
{
	return lhs.min < rhs;
}

}

template< typename T >
class id_pool_interval
{
	using this_type = id_pool_interval<T>;
public:
	using id_type = typename T;

	//--------------------------------
	id_pool_interval():
		intervals_(),
		next_( std::end( intervals_ ) )
	{
		intervals_.emplace_back( T( 0 ), T( 0 ) );
		intervals_.emplace_back( T( max_id() ), T( max_id() ) );
		next_ = std::begin( intervals_ );
	}

	id_pool_interval(this_type const& i_other) = default;
	id_pool_interval(this_type&& i_other) = default;
	id_pool_interval& operator=(this_type const& i_other) = default;
	id_pool_interval& operator=(this_type&& i_other) = default;

	//--------------------------------
	/* new_id()
	*/
	id_type new_id()
	{
		assert( next_ != std::end( intervals_ ) );
		auto& current = *next_;

		if( next_ + 1 != std::end( intervals_ ) )
		{
			assert( intervals_.size() >= 2 );
			auto& next = *( next_ + 1 );

			// check if extending next causes overlap
			assert( next.min > current.max );
			auto const range = static_cast<uint64_t>( next.min ) - static_cast<uint64_t>( current.max );
			assert( range <= max_id() );

			// check if merge is not required
			if( range != 2 )
			{
				// extend
				return ++current.max;
			}
			else // range of two means there is a gap of one in-between the two intervals
			{
				// store the id
				auto id = current.max + T( 1 );

				// merge
				current.max = next.max;
				auto next_itr = intervals_.erase( next_ + 1 );

				// update next
				next_ = next_itr - 1;

				return id;
			}
		}
		else // there is only one interval
		{
			assert( intervals_.size() == 1 );
			assert( intervals_.back().min == 0 && intervals_.back().max == max_id() );
			return max_id();
		}
	}

	//--------------------------------
	/* new_id( i_id )
	*/
	bool new_id( id_type const i_id )
	{
		using namespace detail_interval;
		assert( i_id > id_type( 0 ) && i_id < max_id() );
		auto end = std::end( intervals_ );

		// find nearest interval to i_id
		auto itr = std::lower_bound( std::begin( intervals_ ), std::end( intervals_ ), i_id, operator><T> ); // compare mins instead of max

		if( itr != end )
		{
			assert( intervals_.size() >= 2 );
			auto& current = *itr;
			auto& previous = *( itr - 1 );

			// if its in empty space
			if( i_id > previous.max && i_id < current.min )
			{
				// check if extending previous causes overlap
				auto const range = static_cast<uint64_t>( current.min ) - static_cast<uint64_t>( previous.max );
				assert( range <= max_id() );

				// check if merge is not required
				if( range != 2 )
				{
					// if can extend current
					if( current.min - 1 == i_id )
					{
						--current.min;
						return true;
					}
					// if can extend previous
					else if( previous.max + 1 == i_id )
					{
						++previous.max;
						return true;
					}
					// safe to create a single interval
					else
					{
						// store previous max id
						auto prev_next_id = next_->max;

						// new interval
						auto new_interval = interval_t( i_id, i_id );

						// insert new interval
						auto new_itr = intervals_.insert( itr, std::move( new_interval ) );
						assert( new_itr != std::end( intervals_ ) );

						// update next
						next_ = std::lower_bound( std::begin( intervals_ ), std::end( intervals_ ), prev_next_id );

						return true;
					}
				}
				else // range of two means there is a gap of one in-between the two intervals
				{
					// store previous max id
					auto prev_next_id = next_->max;

					// merge
					previous.max = current.max;
					auto next_itr = intervals_.erase( itr );

					// update next
					next_ = std::lower_bound( std::begin( intervals_ ), std::end( intervals_ ), prev_next_id );

					return true;
				}
			}
			// else it is currently in use
		}
		return false;
	}

	//--------------------------------
	/* delete_id( i_id )
	*/
	bool delete_id( id_type const i_id )
	{
		using namespace detail_interval;
		assert( i_id >= min_id() && i_id < max_id() );

		// find i_id in intervals_
		auto itr = std::lower_bound( std::begin( intervals_ ), std::end( intervals_ ), i_id );

		if( itr != std::end( intervals_ ) )
		{
			auto& current = *itr;

			auto prev_next_id = next_->max;

			// if splitting interval is required
			if( i_id > current.min && i_id < current.max )
			{
				// create upper
				auto upper = interval_t( i_id + T( 1 ), current.max );

				// set lower
				current.max = i_id - T( 1 );

				// insert upper interval
				auto new_itr = intervals_.insert( itr + 1, std::move( upper ) ); 
				// PERFORMANCE: BAD, since the container guarantees to fill up from the lower IDs first (aka low indices)
				// this insertion, into a vector, is worst case possible.

				assert( new_itr != std::end( intervals_ ) );
			}
			// if the interval is 1 in length
			else if( current.min == i_id && current.max == i_id )
			{
				assert( intervals_.size() >= 2 );
				auto new_itr = intervals_.erase( itr );
				assert( new_itr != std::end( intervals_ ) );
			}
			else if( current.min == i_id )
			{
				++current.min;
			}
			else if( current.max == i_id )
			{
				--current.max;
			}
			else// not a used id
			{
				return false;
			}

			// update next
			if( prev_next_id < i_id )
			{
				// if the deleted interval was more than the previous next interval
				// then new_itr is the new next
				next_ = std::lower_bound( std::begin( intervals_ ), std::end( intervals_ ), prev_next_id );
			}
			else
			{
				// if the deleted interval was less than the previous next interval
				// then a new next_ needs to be found (since the iterator has been invalidated)
				next_ = std::lower_bound( std::begin( intervals_ ), std::end( intervals_ ), i_id ) - 1;
			}
		}
		else// not found, id out of range
		{
			assert( false );
			return false;
		}

		return true;
	}

	//--------------------------------
	bool used_id( id_type const i_id ) const
	{
		assert( i_id >= min_id() && i_id < max_id() );

		// find i_id in intervals_
		auto itr = std::lower_bound( std::begin( intervals_ ), std::end( intervals_ ), i_id );

		return itr != std::end( intervals_ ) && i_id >= itr->min && i_id <= itr->max;
	}

	//--------------------------------
	/*
		NOTE: currently working in interval count NOT id count
	*/
	void reserve( size_t const i_size )
	{
		// store previous max id
		auto prev_next_id = next_->max;

		constexpr size_t const max = max_id() / 2; // max size when pathological data set is used
		if( i_size < max )
		{
			intervals_.reserve( i_size ); // TODO: change to an approximation: i_size / average interval length
		}
		else
		{
			intervals_.reserve( max );
		}

		// update next
		next_ = std::lower_bound( std::begin( intervals_ ), std::end( intervals_ ), prev_next_id );
	}

	//--------------------------------
	void shrink_to_fit()
	{
		intervals_.shrink_to_fit();
	}

	//--------------------------------
	size_t capacity() const
	{
		return max_id();
	}

	//--------------------------------
	static constexpr id_type min_id()
	{
		return id_type( 1 );
	}

	//--------------------------------
	static constexpr id_type max_id()
	{
		return std::numeric_limits<id_type>::max();
	}

	//--------------------------------
	template< typename func_t >
	void for_all_used_ids( func_t& i_func ) const
	{
		assert( intervals_.size() > 0 );
		auto itr = std::begin( intervals_ );
		auto end = std::end( intervals_ );

		// for the first interval, skip 0
		auto const first_max= itr->max < max_id() ? itr->max : max_id()-1 ; // NOTE: excludes id_max from being iterated
		for( T i = T( 1 ); i <= first_max; ++i )
		{
			i_func( i );
		}
		++itr;

		// for other intervals
		while( itr != end )
		{
			auto const min = itr->min;
			auto const max = itr->max;
			for( T i = min; i <= max && i < max_id(); ++i )
			{
				i_func( i );
			}
			++itr;
		}
	}

	//--------------------------------
	template< typename func_t >
	void for_all_unused_ids( func_t& i_func ) const
	{
		// for all intervals
		for( size_t i = 0, c = intervals_.size() - 1; i < c; ++i )
		{
			auto& l = intervals_[i];
			auto& h = intervals_[i + 1];
			// for all in-between the two intervals
			for( T j = l.max + 1, k = h.min; j + 1 != max_id() + 1 && j < k; ++j )
			{
				i_func( j );
			}
		}
	}

#if defined( CATCH_CONFIG_MAIN )
	auto const& get_internal_cont() const
	{
		return intervals_;
	}

	auto const get_next() const
	{
		return next_;
	}
#endif

private:

	using interval_t = detail_interval::interval<T>;
	using cont_t = std::vector< interval_t >;
	using cont_itr = typename cont_t::iterator;

	//-------- member variables --------
	cont_t intervals_;
	cont_itr next_;
};