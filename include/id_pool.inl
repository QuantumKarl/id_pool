/*	This file is part of id_pool.
	Copyright(C) 2018 by Karl Wesley Hutchinson

	id_pool is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	id_pool is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with id_pool. If not, see <http://www.gnu.org/licenses/>.
*/
#define id_type typename id_pool<impl_t>::id_type

//-------------------------------- public functions --------------------------------

//--------------------------------
template<typename impl_t>
id_type id_pool<impl_t>::new_id()
{
	return new_id( detail_id_pool::has_new_id_i1<impl_t>() );
}

//--------------------------------
template<typename impl_t>
bool id_pool<impl_t>::new_id( id_type const i_id )
{
	return new_id( i_id, detail_id_pool::has_new_id_i2<impl_t>() );
}

//--------------------------------
template<typename impl_t>
bool id_pool<impl_t>::delete_id( id_type const i_id )
{
	return delete_id( i_id, detail_id_pool::has_delete_id<impl_t>() );
}

//--------------------------------
template<typename impl_t>
bool id_pool<impl_t>::used_id( id_type const i_id ) const
{
	return used_id( i_id, detail_id_pool::has_used_id_i<impl_t>() );
}

//--------------------------------
template<typename impl_t>
void id_pool<impl_t>::reserve( size_t const i_size )
{
	reserve( i_size, detail_id_pool::has_reserve_i<impl_t>() );
}

//--------------------------------
template<typename impl_t>
void id_pool<impl_t>::shrink_to_fit()
{
	shrink_to_fit( detail_id_pool::has_shrink_to_fit_i<impl_t>() );
}

//--------------------------------
template<typename impl_t>
size_t id_pool<impl_t>::capacity() const
{
	return capacity( detail_id_pool::has_capacity_i<impl_t>() );
}

//--------------------------------
template<typename impl_t>
constexpr id_type id_pool<impl_t>::min_id()
{
	return min_id( detail_id_pool::has_min_id_i<impl_t>() );
}

//--------------------------------
template<typename impl_t>
constexpr id_type id_pool<impl_t>::max_id()
{
	return max_id( detail_id_pool::has_max_id_i<impl_t>() );
}

//--------------------------------
template<typename impl_t>
template<typename func_t>
void id_pool<impl_t>::for_all_used_ids( func_t & i_func ) const
{
	for_all_used_ids( i_func, detail_id_pool::has_for_all_used_ids_i<impl_t, func_t>() );
}

//--------------------------------
template<typename impl_t>
template<typename func_t>
void id_pool<impl_t>::for_all_unused_ids( func_t & i_func ) const
{
	for_all_unused_ids( i_func, detail_id_pool::has_for_all_unused_ids_i<impl_t, func_t >() );
}

//-------------------------------- private functions --------------------------------

//--------------------------------
template<typename impl_t>
id_type id_pool<impl_t>::new_id( std::true_type )
{
	return impl_.new_id();
}

//--------------------------------
template<typename impl_t>
id_type id_pool<impl_t>::new_id( std::false_type )
{
	assert( false );
	return id_type();
}

//--------------------------------
template<typename impl_t>
bool id_pool<impl_t>::new_id( id_type const i_id, std::true_type )
{
	return impl_.new_id( i_id )
}

//--------------------------------
template<typename impl_t>
bool id_pool<impl_t>::new_id( id_type const, std::false_type )
{
	assert( false );
	return false;
}

//--------------------------------
template<typename impl_t>
bool id_pool<impl_t>::delete_id( id_type const i_id, std::true_type )
{
	return impl_.delete_id( i_id );
}

//--------------------------------
template<typename impl_t>
bool id_pool<impl_t>::delete_id( id_type const, std::false_type )
{
	assert( false );
	return false;
}

//--------------------------------
template<typename impl_t>
bool id_pool<impl_t>::used_id( id_type const i_id, std::true_type ) const
{
	return impl_.used_id( i_id );
}

//--------------------------------
template<typename impl_t>
bool id_pool<impl_t>::used_id( id_type const, std::false_type ) const
{
	assert( false );
	return false;
}

//--------------------------------
template<typename impl_t>
void id_pool<impl_t>::reserve( size_t const i_size, std::true_type )
{
	impl_.reserve( i_size );
}

//--------------------------------
template<typename impl_t>
void id_pool<impl_t>::reserve( size_t const, std::false_type )
{
	assert( false );
}

//--------------------------------
template<typename impl_t>
void id_pool<impl_t>::shrink_to_fit( std::true_type )
{
	impl_.shrink_to_fit();
}

//--------------------------------
template<typename impl_t>
void id_pool<impl_t>::shrink_to_fit( std::false_type )
{
	assert( false );
}

//--------------------------------
template<typename impl_t>
size_t id_pool<impl_t>::capacity( std::true_type ) const
{
	return impl_.capacity();
}

//--------------------------------
template<typename impl_t>
size_t id_pool<impl_t>::capacity( std::false_type ) const
{
	assert( false );
	return 0;
}

//--------------------------------
template<typename impl_t>
constexpr id_type id_pool<impl_t>::min_id( std::true_type )
{
	return impl_.min_id();
}

//--------------------------------
template<typename impl_t>
constexpr id_type id_pool<impl_t>::min_id( std::false_type )
{
	assert( false );
	return 0;
}

//--------------------------------
template<typename impl_t>
constexpr id_type id_pool<impl_t>::max_id( std::true_type )
{
	return impl_.max_id();
}

//--------------------------------
template<typename impl_t>
constexpr id_type id_pool<impl_t>::max_id( std::false_type )
{
	assert( false );
	return 0;
}

//--------------------------------
template<typename impl_t>
template<typename func_t>
void id_pool<impl_t>::for_all_used_ids( func_t & i_func, std::true_type ) const
{
	impl_.for_all_used_ids( i_func );
}

//--------------------------------
template<typename impl_t>
template<typename func_t>
void id_pool<impl_t>::for_all_used_ids( func_t&, std::false_type ) const
{
	assert( false );
}

//--------------------------------
template<typename impl_t>
template<typename func_t>
void id_pool<impl_t>::for_all_unused_ids( func_t& i_func, std::true_type ) const
{
	impl_.for_all_unused_ids( i_func );
}

//--------------------------------
template<typename impl_t>
template<typename func_t>
void id_pool<impl_t>::for_all_unused_ids( func_t&, std::false_type ) const
{
	assert( false );
}

#undef id_type