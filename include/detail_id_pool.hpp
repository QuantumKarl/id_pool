/*	This file is part of id_pool.
	Copyright(C) 2018 by Karl Wesley Hutchinson

	id_pool is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	id_pool is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with id_pool. If not, see <http://www.gnu.org/licenses/>.
*/
namespace detail_id_pool
{

D_HAS_MEMBER_DETECT( new_id_i1, typename T::id_type, std::declval< T& >( ).new_id( ) )
D_HAS_MEMBER_DETECT( new_id_i2, bool, std::declval< T& >( ).new_id( std::declval< decltype(T::id_type) const >() ) )

D_HAS_MEMBER_DETECT( delete_id, bool, std::declval< T& >( ).delete_id( std::declval< decltype(T::id_type) const >() ) )

D_HAS_MEMBER_DETECT( used_id_i, bool, std::declval< T const& >( ).used_id( std::declval< decltype(T::id_type) const >() ) )

D_HAS_MEMBER_DETECT( reserve_i, void, std::declval< T& >( ).reserve( std::declval< size_t const >() ) )
D_HAS_MEMBER_DETECT( shrink_to_fit_i, void, std::declval< T& >( ).shrink_to_fit( ) )

D_HAS_MEMBER_DETECT( capacity_i, size_t, std::declval< T const& >( ).capacity() )

D_HAS_MEMBER_DETECT( min_id_i, typename T::id_type, std::declval< T& >( ).min_id( ) )
D_HAS_MEMBER_DETECT( max_id_i, typename T::id_type, std::declval< T& >( ).max_id( ) )

D_HAS_MEMBER_DETECT_1( for_all_used_ids_i, void, std::declval< T const& >( ).for_all_used_ids( std::declval< X& >() ) )
D_HAS_MEMBER_DETECT_1( for_all_unused_ids_i, void, std::declval< T const& >( ).for_all_unused_ids( std::declval< X& >() ) )

#include "id_pool_interval.inl"
#include "id_pool_bitset.inl"

}