/*	This file is part of id_pool.
	Copyright(C) 2018 by Karl Wesley Hutchinson

	id_pool is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	id_pool is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with id_pool. If not, see <http://www.gnu.org/licenses/>.
*/
#if !defined( D_ID_POOL_HPP )
#define D_ID_POOL_HPP

#include <cassert>
#include <limits>
#include <algorithm>
#include <vector>
#include <cstdint>

#include <type_traits>
#include <array>
#include <bitset.hpp>

#include <sys_SFINAE.hpp>
#include "detail_id_pool.hpp"

template< typename impl_t >
class id_pool
{
public:
	using id_type = typename impl_t::id_type;

	id_type new_id();
	bool new_id( id_type const i_id );

	bool delete_id( id_type const i_id );

	bool used_id( id_type const i_id ) const;

	void reserve( size_t const i_size );
	void shrink_to_fit();

	size_t capacity() const;

	static constexpr id_type min_id();
	static constexpr id_type max_id();

	template< typename func_t >
	void for_all_used_ids( func_t& i_func ) const;

	template< typename func_t >
	void for_all_unused_ids( func_t& i_func ) const;

private:
	//--------------------------------
	id_type new_id( std::true_type );
	id_type new_id( std::false_type );

	//--------------------------------
	bool new_id( id_type const i_id, std::true_type );
	bool new_id( id_type const i_id, std::false_type );

	//--------------------------------
	bool delete_id( id_type const i_id, std::true_type );
	bool delete_id( id_type const i_id, std::false_type );

	//--------------------------------
	bool used_id( id_type const i_id, std::true_type ) const;
	bool used_id( id_type const i_id, std::false_type ) const;

	//--------------------------------
	void reserve( size_t const i_size, std::true_type );
	void reserve( size_t const i_size, std::false_type );

	//--------------------------------
	void shrink_to_fit(std::true_type);
	void shrink_to_fit(std::false_type);

	//--------------------------------
	size_t capacity(std::true_type) const;
	size_t capacity(std::false_type) const;

	//--------------------------------
	static constexpr id_type min_id(std::true_type);
	static constexpr id_type min_id(std::false_type);

	//--------------------------------
	static constexpr id_type max_id(std::true_type);
	static constexpr id_type max_id(std::false_type);

	//--------------------------------
	template< typename func_t >
	void for_all_used_ids( func_t& i_func, std::true_type ) const;
	template< typename func_t >
	void for_all_used_ids( func_t& i_func, std::false_type) const;

	//--------------------------------
	template< typename func_t >
	void for_all_unused_ids( func_t& i_func, std::true_type ) const;
	template< typename func_t >
	void for_all_unused_ids( func_t& i_func, std::false_type ) const;

	//--------------------------------
	impl_t impl_;
};

#include "id_pool.inl"

template< typename id_type >
using id_pool_interval = detail_id_pool::id_pool_interval<id_type>;

template< size_t id_bit_count = 22, size_t index_bit_count = 10>
using id_pool_bitset = detail_id_pool::id_pool_bitset<id_bit_count,index_bit_count>;

#endif