/*	This file is part of id_pool.
	Copyright(C) 2018 by Karl Wesley Hutchinson

	id_pool is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	id_pool is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with id_pool. If not, see <http://www.gnu.org/licenses/>.
*/

// NOTE: an allocator that helps make sure memory is allocated sequentially would improve performance OR
// make sure one entry is the size of the L1 cache

// NOTE: impractical to use larger id counts, so uint32_t is used (and not templated).

//--------------------------------
template< size_t id_bit_count, size_t index_bit_count >
class id_pool_bitset
{
	static constexpr uint32_t const total_bits = sizeof(uint32_t) * 8;
	static constexpr uint32_t const all_set_mask = static_cast<uint32_t>(~0u);

	static constexpr uint32_t const bit_count = 1u << id_bit_count;
	static constexpr uint32_t const array_count = 1u << index_bit_count;
	static constexpr uint32_t const bits_count = id_bit_count + index_bit_count;

	static_assert( bits_count <= total_bits, "id_pool, only supports 32 bit indices" );

	static constexpr uint32_t const index_mask = (all_set_mask >> (total_bits - bits_count)) & (all_set_mask << id_bit_count);
	static constexpr uint32_t const id_mask = all_set_mask >> (total_bits- id_bit_count);
	static constexpr uint32_t const max_id_mask = all_set_mask >> (total_bits - bits_count);

	// assert no overlap is in the bit masks
	static_assert( (index_mask ^ id_mask) == max_id_mask, "bit mask error in id_pool_bitset");

	using cont_t = bitset_tiered< bit_count >;

	using this_type = id_pool_bitset<id_bit_count, index_bit_count>;
public:
	using id_type = typename uint32_t;

	//--------------------------------
	id_pool_bitset():
		bitset_array_(),
		next_index_(0),
		next_id_(0)
	{
		memset( &bitset_array_[0], 0, sizeof( cont_t* ) * array_count );
	}

	//--------------------------------
	~id_pool_bitset()
	{
		for( auto ptr : bitset_array_ )
		{
			if( ptr != nullptr )
			{
				delete ptr;
			}
		}
		memset( &bitset_array_[0], 0, sizeof( cont_t* ) * array_count );
	}

	// TODO: operators
	id_pool_bitset(this_type const& i_other) = delete;
	id_pool_bitset(this_type&& i_other) = delete;
	id_pool_bitset& operator=(this_type const& i_other) = delete;
	id_pool_bitset& operator=(this_type&& i_other) = delete;

	//--------------------------------
	/* new_id()
	*/
	id_type new_id()
	{
		const uint32_t prev_index = next_index_;
		const uint32_t prev_id = next_id_;
		// update next for next new id request
		// NOTE: update next can fail here, however failure
		// would indicate the next insertion would fail,
		// not this one
		bool updated = update_next( prev_index, prev_id );

		if( updated == true )
		{
			const uint32_t new_index = next_index_;
			const uint32_t new_id = next_id_;
			assert( new_id < id_range() );

			// assert it is free
			assert( bitset_array_[new_index]->read_at( new_id ) == true );

			// set the bit to used
			bitset_array_[new_index]->write_at( new_id, std::false_type() );

			// create the new id
			return (new_index << id_bit_count) | new_id;
		}
		// failure
		return max_id();
	}

	//--------------------------------
	/* new_id( i_id )
	*/
	bool new_id( id_type const i_id )
	{
		uint32_t index = 0;
		uint32_t id = 0;

		id_convert(i_id, index, id);
		
		if( index < index_range() && id < id_range() )
		{
			if( bitset_array_[index] != nullptr )
			{
				// if it isnt used
				if( bitset_array_[index]->read_at( id ) == true )
				{
					assert( index >= next_index_ );
					bitset_array_[index]->write_at( id, std::false_type() );

					// if index is greater than next_index we don't care,
					// NOTE: its not possible to be less than next_index_
					if( (index == next_index_ && id == next_id_) == false )
					{
						return true;
					}
					else
					{
						return update_next( index, next_id_ );
					}
				}
				// else in use
				return false;
			}
			else
			{
				e_expand const res = expand( index );
				if( res == e_expand::expanded )
				{
					bitset_array_[index]->write_at(id, std::false_type());
					if( index > next_index_ )
					{
						return true;
					}
					else
					{
						return update_next( index, 0 );
					}
				}
				// else failed, out of memory
				return false;
			}
		}

		return false;
	}

	//--------------------------------
	/* delete_id( i_id )
	*/
	bool delete_id( id_type const i_id )
	{
		uint32_t index = 0;
		uint32_t id = 0;

		id_convert(i_id, index, id);

		if( index < index_range() &&
			id < id_range() &&
			bitset_array_[index] != nullptr )
		{
			// if it is used
			if( bitset_array_[index]->read_at( id ) == false )
			{
				// set it to free
				bitset_array_[index]->write_at( id, std::true_type() );

				// if index is less than next_index its the new next
				if( index < next_index_ )
				{
					next_index_ = index;
					next_id_ = id;
					return true;
				}
				else if( index == next_index_ && id <= next_id_ )
				{
					return update_next( index, id );
				}
				else // greater than next_index_
				{
					return true;
				}
			}
			// else not in use
			return false;
		}
		return false;
	}

	//--------------------------------
	bool used_id( id_type const i_id ) const
	{
		uint32_t index = 0;
		uint32_t id = 0;

		id_convert(i_id, index, id);

		if( index < index_range() &&
			id < id_range() &&
			bitset_array_[index] != nullptr )
		{
			// false is used
			return bitset_array_[index]->read_at( id ) == false;
		}

		return false;
	}

	//--------------------------------
	/*
		NOTE: working in id count units
	*/
	void reserve( size_t const i_size )
	{
		size_t stacks = static_cast<size_t>( static_cast<double>( i_size ) / static_cast<double>( id_range() ) + 0.5 );
		for( size_t i = 0; i < array_count && i < stacks; ++i )
		{
			if( bitset_array_[i] == nullptr )
			{
				e_expand const res = expand( i );
				if( res != e_expand::expanded )
				{
					assert( false );
					return;
				}
			}
			--stacks;
		}
	}

	//--------------------------------
	void shrink_to_fit()
	{
		for( size_t i = 0; i < array_count ; ++i )
		{
			auto* ptr = bitset_array_[i];
			if( ptr != nullptr && back->none() == true )
			{
				delete ptr;
				bitset_array_[i] = nullptr;
			}
		}
	}

	//--------------------------------
	size_t capacity() const
	{
		size_t sum = 0;
		for( size_t i = 0; i < array_count ; ++i )
		{
			sum += bitset_array_[i] != nullptr ? id_range() : 0 ;
		}

		return sum;
	}

	//--------------------------------
	static constexpr id_type min_id()
	{
		return id_type(0);
	}

	//--------------------------------
	static constexpr id_type max_id()
	{
		return max_id_mask;
	}

	//--------------------------------
	template< typename func_t >
	void for_all_used_ids( func_t& i_func ) const
	{
		// for all valid bitset entries
		for (size_t i = 0; i < array_count; ++i)
		{
			id_type const index = i << id_bit_count;
			if (bitset_array_[i] != nullptr)
			{
				auto& bitset = *bitset_array_[i];
				constexpr id_type const count = bitset.size();

				// TODO: call / bitset.add for_all_set(i_func)
				for (id_type j = 0 ; j < count; ++j)
				{
					// if it is used
					if (bitset.read_at(j) == false)
					{
						id_type const id = index | j;

						// assert no overlap
						assert((index | j) == (index ^ j));
						i_func(id);
					}
				}
			}
		}
	}

	//--------------------------------
	template< typename func_t >
	void for_all_unused_ids( func_t& i_func ) const
	{
		// for all valid bitset entries
		for (size_t i = 0; i < array_count; ++i)
		{
			id_type const index = i << id_bit_count;
			if (bitset_array_[i] != nullptr)
			{
				auto& bitset = *bitset_array_[i];

				// TODO: call / add bitset.for_all_unset(i_func)
				for (id_type j = 0; j < bit_count; ++j)
				{
					// if it is not used
					if (bitset.read_at(j) == true)
					{
						id_type const id = index | j;
						// assert no overlap
						assert((index | j) == (index ^ j));
						i_func(id);
					}
				}
			}
			else // all of the this region is unused, pretend that it is there
			{
				for (id_type j = 0; j < bit_count; ++j)
				{
					id_type const id = index ^ j;
					i_func(id);
				}
			}
		}
	}

#if defined( CATCH_CONFIG_MAIN )
	auto const& get_internal_cont() const
	{
		return bitset_array_;
	}
#endif

private:

	//--------------------------------
	enum class e_expand : size_t
	{
		not_expanded = 0,
		expanded,
		out_of_memory
	};

	//--------------------------------
	static constexpr uint32_t id_range()
	{
		return bit_count;
	}

	//--------------------------------
	static constexpr uint32_t index_range()
	{
		return array_count;
	}

	//--------------------------------
	static void id_convert( uint32_t const i_id, uint32_t& o_index, uint32_t& o_id)
	{
		o_index = (i_id & index_mask) >> id_bit_count;
		o_id = i_id & id_mask;
	}

	//--------------------------------
	/* update_next( i_idex, i_id )
		updates the members: next_index_ and next_id_, to the next gap in the bitsets

		TODO: maybe remove quick check, benchmark
		TODO: make static / const
	*/
	bool update_next( uint32_t const i_index, uint32_t const i_id )
	{
		// quick check(most probable branch)
		if( bitset_array_[i_index] != nullptr )
		{
			size_t const id = bitset_array_[i_index]->bsf(); // TODO: optimistation: bitscan from bitset_array_[i][i_id]
			if( id != cont_t::npos() )
			{
				next_id_ = id;
				next_index_ = i_index;
				return true;
			}
		}
		// failed quick check
		size_t i = i_index;
		do
		{
			if( bitset_array_[i] != nullptr )
			{
				size_t const id = bitset_array_[i]->bsf();
				if( id != cont_t::npos() )
				{
					next_id_ = id;
					next_index_ = i;
					return true;
				}
				// else bitset_array_[i] is full, thus check next
				continue;
			}
			else
			{
				e_expand const res = expand( i );
				if( res == e_expand::expanded )
				{
					next_id_ = 0u;
					next_index_ = i;
					return true;
				}
				else // failed to expand
				{
					return false;
				}
			}
		} while( ++i < array_count );
		return false;
	}

	//--------------------------------
	// expand
	e_expand expand( uint32_t const i_index )
	{
		if( i_index < array_count )
		{
			if( bitset_array_[i_index] == nullptr )
			{
				bitset_array_[i_index] = new cont_t();
				bitset_array_[i_index]->one_all();

				return e_expand::expanded;
			}
			return e_expand::not_expanded;
		}
		// failed, out of memory
		return e_expand::out_of_memory;
	}

	std::array<cont_t*, array_count> bitset_array_;
	uint32_t next_index_;
	uint32_t next_id_;
};