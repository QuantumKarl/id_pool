# id_pool
This project contains a single container that holds a pool of IDs (similar to a set), it provides functions such as:

 - `new_id(); // set/return a new id`
 - `new_id(id_value); // set a new id with id_value`
 - `delete_id(id_value); // delete id at id_value`
 - `used_id(id_value); // query if id_value is used`
 
 The purpose of this container is to reuse IDs (instead of incrementing an integer until it overflows), in addition the container saves memory.
 
## Getting Started
To use id_pool as part of your project, add its "include" folder to the additional includes on your project and then `#include "id_pool.hpp"`.

## Dependencies
This project depends on:

 - C++14 compiler
 - [bitset](https://bitbucket.org/QuantumKarl/bitset/src/master/)
 - Catch2
 - hayai
 
All external dependencies are header only, they are resolved by inclusion into the project via GIT sub-modules.

## Running the tests / benchmark
Currently id_pool only has an msvs2015 sln file, it contains two projects:

 - `id-pool` - this project contains the unit test code
 - `id-pool-bench` - this project contains the benchmark code

## Performance
There are two different implementations of this container:

 1. [id pool interval](https://bitbucket.org/QuantumKarl/id_pool/src/master/include/id_pool_interval.inl) - uses a set of mins and maxes to record which IDs are used
 2. [id pool bitset](https://bitbucket.org/QuantumKarl/id_pool/src/master/include/id_pool_bitset.inl) - uses a bitset to record which IDs are used

Below is a comparison of their performance (log base 2 scale):
![elapsed time comparison](https://bitbucket.org/QuantumKarl/id_pool/raw/362d2a5cd3c497c2a6dd319988d4a51b12ec0761/doc/total_time_chart.png)

As you can see, id_pool_interval is significantly slower, this is due to id_pool_interval using the std::vector container. In the delete_id function (and others but delete_id is worst),
it inserts into the vector at the lowest gap between the intervals, this is worst case for vector insert and delete (since it has to
move close to the maximum number of elements).

This problem could be alleviated by flipping the sorting order of the container. However, as the container gets more full,
it will slow down to even greater amounts than it does currently.

Therefore, I am not going to attempt to improve id_pool_interval because:

 - id_pool_bitset suits my future planned project better
 - it does save more memory than id_pool_bitset, and any algorithmic changes may alter that
 - it has a pathological data set of: fill the container, remover every other value (ie, all even IDs or all odd IDs, much like memory fragmentation)
 - you cant optimise for everything

## TODO

 - Create a meson build for linux
 - Pass verification test (Dr. Memory passed already)
 - Thread safe version(?)
 - Benchmark against an incremented integer / dummy implementation