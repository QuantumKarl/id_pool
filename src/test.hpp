/*	This file is part of id_pool.
	Copyright(C) 2018 by Karl Wesley Hutchinson

	id_pool is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	id_pool is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with id_pool. If not, see <http://www.gnu.org/licenses/>.
*/
//--------------------------------
enum class e_functions : size_t
{
	// 60% chance that a new id will be added
	new_id1 = 0,
	new_id_valid,
	new_id_random,
	delete_id_valid,
	delete_id_random,
	count
};

//--------------------------------
template< typename id_pool_cont, typename id_type >
class test_args
{
public:
	using id_pool_t = id_pool_cont;

	test_args( id_pool_t& io_ids,
			   set_flat<id_type>& io_used_ids,
			   std::mt19937_64& io_gen1,
			   std::uniform_int_distribution<uint64_t>& io_dist,
			   id_type const& io_min,
			   id_type const& io_max):
		ids(io_ids),
		used_ids(io_used_ids),
		gen1(io_gen1),
		distID(io_dist),
		min(io_min),
		max(io_max)
	{ }

	id_pool_t& ids;
	set_flat<id_type> used_ids;
	std::mt19937_64& gen1;
	std::uniform_int_distribution<uint64_t>& distID;
	id_type const& min;
	id_type const& max;
};

namespace id_interval_test
{

//--------------------------------
// find the first empty gap
template< typename T >
auto const find_next( std::vector<T> const& i_intervals )
{
	// for all intervals
	for( size_t i = 0, c = i_intervals.size() - 1; i < c; ++i )
	{
		auto& l = i_intervals[i];
		auto& h = i_intervals[i + 1];
		if( h.min - l.max >= 2 )
		{
			return std::begin( i_intervals ) + i;
		}
	}
	return std::begin( i_intervals );
}

//--------------------------------
template< typename T, typename id_t >
auto pick_empty_gap( std::mt19937_64& gen1, id_pool_interval<T> const& i_pool, id_t& o_id )
{
	auto const& intervals = i_pool.get_internal_cont();
	if( intervals.size() < 2) return false;

	std::uniform_real_distribution<double> dist( 0.0, 1.0 );
	// the number of intervals is equal to the number of gaps
	double const chance = 1.0 / static_cast<double>(intervals.size());

	// for all intervals
	typename detail_id_pool::detail_interval::interval<uint64_t> prev;
	for( size_t i = 0, c = intervals.size() - 1; i < c; ++i )
	{
		auto& l = intervals[i];
		auto& h = intervals[i + 1];
		if( h.min - l.max >= 2 )
		{
			prev.min = l.max;
			prev.max = h.min;
			// random chance to take this gap
			double const value = dist( gen1 );
			if( value < chance )
			{
				break;
			}
		}
	}
	std::uniform_int_distribution<size_t> dist_gap(prev.min + 1, prev.max - 1);
	o_id = id_t(dist_gap(gen1));
	return true; 
}

//--------------------------------
template< typename T >
void check_valid( id_pool_interval<T> const& id_pool, set_flat<T> const& used_ids )
{
	using namespace detail_id_pool;
	using namespace detail_id_pool::detail_interval;

	// check the internal contents are valid
	{
	#if defined( D_PRINT_INTERVALS ) && (D_PRINT_INTERVALS == 1)
		std::cout << "\n------\n{\n";
	#endif
		auto const& intervals = id_pool.get_internal_cont();
		for( size_t i = 0, c = intervals.size() - 1; i < c; ++i )
		{
			auto& current = intervals[i];
			auto& next = intervals[i + 1];
		#if defined( D_PRINT_INTERVALS ) && (D_PRINT_INTERVALS == 1)
			std::cout << '{' << uint64_t( current.min ) << ',' << uint64_t( current.max ) << "},\n{"
				<< uint64_t( next.min ) << ',' << uint64_t( next.max ) << "}\n";
		#endif
			REQUIRE( current.min <= current.max );
			REQUIRE( next.min <= next.max );
			REQUIRE( current.max < next.min );
			REQUIRE( next.min - current.max >= 2 );
		}
	#if defined( D_PRINT_INTERVALS ) && (D_PRINT_INTERVALS == 1)
		std::cout << "}" << std::endl;
	#endif

		// compare the internal next insert location against a computed one
		auto const itr1 = id_pool.get_next();
		auto const itr2 = find_next( intervals );
		if( itr1 != itr2 )
		{
			std::cout << "fail" << std::endl;
		}
		REQUIRE( itr1 == itr2 );
	}

	// compare iterated used ids vs our tracked sets of used ids
	{
		auto func1 = [&] ( T id ) {
			bool const used = used_ids.find( id ) != used_ids.end();
			REQUIRE( used == true );
		};
		id_pool.for_all_used_ids( func1 );
	}

	// compare iterated unused ids vs our tracked sets of used ids
	{
		auto func2 = [&] ( T id ) {
			bool const used = used_ids.find( id ) != used_ids.end();
			REQUIRE( used == false );
			};
		id_pool.for_all_unused_ids( func2 );
	}
}

}// end of namespace id_interval_test

namespace id_bitset_test
{

//--------------------------------
// find the first empty gap
template< typename T, size_t size >
void find_next( std::array<T,size> const& bitset_array, size_t& next_id, size_t& next_index )
{
	size_t index = 0;
	for (auto& ptr : bitset_array)
	{
		if (ptr != nullptr)
		{
			auto& bitset = *ptr;
			for (size_t i = 0, c = bitset.size(); i < c; ++i)
			{
				// if it isnt used
				if (bitset.read_at(i) == true)
				{
					next_id = i;
					next_index = index;
					return;
				}
			}
		}
		++index;
	}
}

//--------------------------------
template< size_t id_count_t, size_t index_count_t, typename id_t >
auto pick_empty_gap(std::mt19937_64& gen1, id_pool_bitset<id_count_t, index_count_t> const& i_pool, id_t& o_id)
{
	auto const& bitset_array = i_pool.get_internal_cont();
	auto const size = bitset_array.size();

	size_t const count = std::count_if(begin(bitset_array), end(bitset_array), [](auto* ptr) {return ptr != nullptr; });
	if (count < 2) return false;

	std::uniform_real_distribution<double> dist(0.0, 1.0);

	double const outer_chance = 1.0 / static_cast<double>(count);

	bool selected = false;
	size_t id = 0;
	size_t index = 0;

	// for all intervals
	for( size_t i = 0; i < size && selected == false; ++i )
	{
		auto& ptr = bitset_array[i];
		if (ptr != nullptr)
		{
			// random chance to take this gap
			if( dist(gen1) < outer_chance )
			{
				auto& bitset = *ptr;
				double const inner_chance = 1.0 / static_cast<double>(bitset.size());
				for(size_t j = 0, c = bitset.size(); j < c && selected == false; ++j)
				{
					// if it isnt used
					if (bitset.read_at(j) == true )
					{
						id = j;
						index = i;
						// if its a winner
						if (dist(gen1) < inner_chance)
						{
							// exit loops
							selected = true;
							break;
						}
					}
				}
			}
		}
	}

	if (selected == false)
	{
		// if the code gets to here random chance has failed us, so pick the first gap
		find_next(bitset_array, id, index);
	}

	o_id = (index << id_count_t) | id;
	return true;
}

//--------------------------------
template< size_t id_count_t, size_t index_count_t, typename T >
void check_valid( id_pool_bitset<id_count_t,index_count_t> const& id_pool, set_flat<T> const& used_ids )
{
	auto bitset_array = id_pool.get_internal_cont();

	// compare iterated used ids vs our tracked sets of used ids
	auto func1 = [&](T id) {
		bool const used = used_ids.find(id) != used_ids.end();
		if (used == false)
		{
			std::cout << "fail" << std::endl;
		}
		REQUIRE(used == true);
	};
	id_pool.for_all_used_ids(func1);

	// compare iterated unused ids vs our tracked sets of used ids
	auto func2 = [&](T id) {
		bool const used = used_ids.find(id) != used_ids.end();
		if (used == true)
		{
			std::cout << "fail" << std::endl;
		}
		REQUIRE(used == false);
	};
	id_pool.for_all_unused_ids(func2);
}

}// end of namespace id_bitset_test

// NOTE: to test a different container, another namespace should be created with overloaded functions

//--------------------------------
template<typename T >
void new_id(T& args)
{
	auto id = args.ids.new_id();
	args.used_ids.insert(id);

	REQUIRE(id != T::id_pool_t::max_id());
	using namespace id_bitset_test;
	using namespace id_interval_test;
	check_valid(args.ids, args.used_ids);
}

//--------------------------------
template< typename T >
void new_id_valid(T& args)
{
	auto id = T::id_pool_t::id_type(0);
	using namespace id_bitset_test;
	using namespace id_interval_test;
	auto valid = pick_empty_gap(args.gen1, args.ids, id);
	if (valid == false) return;

	bool const used = args.used_ids.find(id) != args.used_ids.end();
	REQUIRE(used == false);

	bool const added = args.ids.new_id(id);
	REQUIRE(added == true);
	args.used_ids.insert(id);
	check_valid(args.ids, args.used_ids);
}

//--------------------------------
template< typename T >
void new_id_random( T& args)
{
	if (args.used_ids.size() == 0) return;
	auto const id = T::id_pool_t::id_type(args.distID(args.gen1));
	if (id <= args.min || id >= args.max) return;

	bool const used = args.used_ids.find(id) != args.used_ids.end();
	bool const added = args.ids.new_id(id);
	REQUIRE(added != used);
	if (added == true)
	{
		args.used_ids.insert(id);
	}
	using namespace id_bitset_test;
	using namespace id_interval_test;
	check_valid(args.ids, args.used_ids);
}

//--------------------------------
template< typename T >
void delete_id_valid( T& args)
{
	if (args.used_ids.size() == 0) return;
	std::uniform_int_distribution<size_t> dist_index(0, args.used_ids.size() - 1);
	// pick a random index
	size_t const index = dist_index(args.gen1);
	T::id_pool_t::id_type const id = args.used_ids[index];
	if (id <= args.min || id >= args.max) return;

	bool const used = args.used_ids.find(id) != args.used_ids.end();
	REQUIRE(used == true);
	bool const removed = args.ids.delete_id(id);
	REQUIRE(used == removed);
	if (removed == true)
	{
		args.used_ids.erase(id);
	}
	using namespace id_bitset_test;
	using namespace id_interval_test;
	check_valid(args.ids, args.used_ids);
}

//--------------------------------
template< typename T >
void delete_id_random( T& args)
{
	if (args.used_ids.size() == 0) return;
	auto const id = T::id_pool_t::id_type( args.distID(args.gen1) );
	if (id <= args.min || id >= args.max) return;

	bool const used = args.used_ids.find(id) != args.used_ids.end();
	bool const removed = args.ids.delete_id(id);
	REQUIRE(removed == used);
	if (removed == true)
	{
		args.used_ids.erase(id);
	}
	using namespace id_bitset_test;
	using namespace id_interval_test;
	check_valid(args.ids, args.used_ids);
}

//--------------------------------
/* all_random_test()
	Test all functionality by randomly selecting what
	function to test, then randomly selecting input.
*/
template< size_t num_func_calls, typename id_pool_t >
void all_random_test()
{
	using namespace id_interval_test;
	using namespace id_bitset_test;
	constexpr size_t const min = id_pool_t::min_id();
	constexpr size_t const max = id_pool_t::max_id();

	std::mt19937_64 gen1;
	std::uniform_int_distribution<uint64_t> distID( min, max - 1 ); // NOTE: excludes max id
	std::uniform_int_distribution<size_t> distFunc( size_t( 0 ), size_t( e_functions::count ) );

	id_pool_t ids;
	set_flat< id_pool_t::id_type > used_ids;
	used_ids.reserve( 1024 );

	test_args<id_pool_t,id_pool_t::id_type> args( ids, used_ids, gen1, distID, min, max );
	for( size_t i = 0; i < num_func_calls; ++i )
	{
		switch( distFunc( gen1 ) )
		{
			case size_t( e_functions::new_id1 ):
				new_id( args );
			break;

			case size_t( e_functions::new_id_valid ):
				new_id_valid( args );
			break;

			case size_t( e_functions::new_id_random ):
				new_id_random( args );
			break;

			case size_t( e_functions::delete_id_valid ):
				delete_id_valid( args );
			break;

			case size_t( e_functions::delete_id_random ):
				delete_id_random( args );
			break;

			default:
				break;
		}
	}
}