/*	This file is part of id_pool.
	Copyright(C) 2018 by Karl Wesley Hutchinson

	id_pool is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	id_pool is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with id_pool. If not, see <http://www.gnu.org/licenses/>.
*/
#include <fstream>
#include <hayai.hpp>
#include <random>
#include <string>
#include <vector>

#include "../include/id_pool.hpp"

#define D_INTERVAL 1
#define D_BITSET 0

#if defined( D_INTERVAL ) && ( D_INTERVAL == 1 )
using id_pool_cont = id_pool_interval< uint32_t >; // WARNING: estimated 24 hours run time
std::string const file_name = "id_pool_interval_benchmark.json";
#elif defined( D_BITSET ) && ( D_BITSET == 1 )
using id_pool_cont          = id_pool_bitset< 22, 10 >;
std::string const file_name = "id_pool_bitset_benchmark.json";
#else
static_assert( false, "unknown id_pool_selection" );
#endif

//----------------------------------------------------------------
class IdPoolFixture : public ::hayai::Fixture
{
    static constexpr size_t id_count      = 1u << 23;
    static constexpr double removed_ratio = 1.0 - 1.0 / 3.0;
    static constexpr size_t remove_count =
        static_cast< size_t >( static_cast< double >( id_count ) * removed_ratio + 0.5 );
    static constexpr double removed_deviation = removed_ratio / 2.0;
    static constexpr size_t pattern_count     = 4;

  public:
    //--------------------------------
    IdPoolFixture( )
        : pattern_index_( 0 )
    {
    }

    //--------------------------------
    /* SetUp()
		creates a vector of vectors where the sub-vector
		contains a set of numbers called a "pattern".
		this pattern is a set of ids to be removed.
	*/
    void
        SetUp( ) override
    {
        std::mt19937_64 gen( 1337 ); // constant seed to make test as fair as possible

        std::vector< size_t > pattern;
        pattern.reserve( remove_count );

        // generate ids up to remove_count
        for ( size_t i = id_pool_cont::min_id( ); i < id_count; ++i )
        {
            pattern.emplace_back( i );
        }

        std::uniform_real_distribution< double > remove_dist( 0.0, removed_deviation );

        // generate patterns
        for ( size_t i = 0; i < pattern_count; ++i )
        {
            auto pat = pattern; // copy
            shuffle( pat, gen );
            size_t difference = remove_count;

            // to make it more varied remove a random amount on top of the standard amount
            difference += static_cast< size_t >(
                remove_dist( gen ) * static_cast< double >( difference ) + 0.5 );
            while ( difference != 0 )
            {
                pat.pop_back( );
                --difference;
            }

            // store the pattern for later use
            patterns_.emplace_back( pat ); // move
        }

        // fill the id_pool
        for ( size_t i = 0; i < id_count; ++i )
        {
            ids.new_id( );
        }

        std::cout << "setup complete" << std::endl;
    }

    //--------------------------------
    void
        TearDown( ) override
    {
        patterns_.clear( );
    }

    //--------------------------------
    void
        Test( )
    {
        // get current removal pattern
        auto const& pattern = get_current_pattern( );

        // remove a full set of patterns
        for ( auto const id : pattern )
        {
            ids.delete_id( id );
        }

        // fill the ids back in (sequentially)
        for ( size_t i = 0, c = pattern.size( ); i < c; ++i )
        {
            ids.new_id( );
        }

        // to make sure it doesn't optimise out
        if ( ids.used_id( ids.max_id( ) - 1 ) )
        {
            std::cout << "unreachable" << std::endl;
        }
    }

  private:
    //--------------------------------
    std::vector< size_t >&
        get_current_pattern( )
    {
        auto const current = pattern_index_;
        ++pattern_index_;
        if ( pattern_index_ >= pattern_count )
        {
            pattern_index_ = 0;
        }
        return patterns_[current];
    }

    //--------------------------------
    template < typename container_t >
    void
        shuffle( container_t& io_cont, std::mt19937_64& i_gen )
    {
        // Simple Fisher Yates shuffle.
        size_t const                              size = io_cont.size( );
        std::uniform_int_distribution< uint64_t > dist_index( 0, size - 1 );
        // Swap each value with a random index,
        // each element will be swapped at least once
        for ( size_t i = 0, index = 0; i < size; ++i )
        {
            // Get index to swap
            index = static_cast< size_t >( dist_index( i_gen ) );

            // Swap values
            if ( index != i )
            {
                std::swap( io_cont[i], io_cont[index] );
            }
        }
    }

    //--------------------------------
    id_pool_cont                         ids;
    std::vector< std::vector< size_t > > patterns_;
    size_t                               pattern_index_;
};

constexpr size_t runs       = 2;
constexpr size_t iterations = 54;

BENCHMARK_F( IdPoolFixture, Pattern, runs, iterations )
{
    Test( );
}

//----------------------------------------------------------------
int
    main( )
{
    // start tests
    std::fstream         jsonFile( file_name, std::fstream::out );
    hayai::JsonOutputter jsonOutputter( jsonFile );
    hayai::Benchmarker::AddOutputter( jsonOutputter );

    hayai::ConsoleOutputter consoleOutputter;
    hayai::Benchmarker::AddOutputter( consoleOutputter );

    hayai::Benchmarker::RunAllTests( );

    return EXIT_SUCCESS;
}
