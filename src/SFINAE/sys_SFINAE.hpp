/*	This file is part of id_pool.
	Copyright(C) 2018 by Karl Wesley Hutchinson

	id_pool is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	id_pool is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with id_pool. If not, see <http://www.gnu.org/licenses/>.
*/
#if !defined( D_SYS_SINFAE_HPP )
#define D_SYS_SINFAE_HPP

#include <type_traits>
#include <utility>

//----------------------------------------------------------------
// below is functionality of C++17 implemented in C++14
// (Google each struct name and template name to learn more)

#ifndef _MSC_VER
namespace std
{
template < typename... >
using void_t = void;
}
#endif

struct nonesuch
{
    nonesuch( )                 = delete;
    ~nonesuch( )                = delete;
    nonesuch( nonesuch const& ) = delete;
    void operator=( nonesuch const& ) = delete;
};

namespace detail_detector
{
template < class Default, class AlwaysVoid, template < class... > class Op, class... Args >
struct detector
{
    using value_t = std::false_type;
    using type    = Default;
};

template < class Default, template < class... > class Op, class... Args >
struct detector< Default, std::void_t< Op< Args... > >, Op, Args... >
{
    using value_t = std::true_type;
    using type    = Op< Args... >;
};

} // namespace detail_detector

template < template < class... > class Op, class... Args >
using is_detected = typename detail_detector::detector< nonesuch, void, Op, Args... >::value_t;

template < template < class... > class Op, class... Args >
using detected_t = typename detail_detector::detector< nonesuch, void, Op, Args... >::type;

template < class Default, template < class... > class Op, class... Args >
using detected_or = detail_detector::detector< Default, void, Op, Args... >;

template < class Expected, template < class... > class Op, class... Args >
using is_detected_exact = std::is_same< Expected, detected_t< Op, Args... > >;

// end of C++17 features
//----------------------------------------------------------------

//----------------------------------------------------------------
// SFINAE member / functionality detection

#define D_HAS_MEMBER_DETECT( id, ret, expr ) \
template< class T >                          \
using id##_t = decltype( expr );             \
 \
template< typename T >                       \
using has_##id = is_detected_exact< ret, id##_t, T >;

#define D_HAS_MEMBER_DETECT_1( id, ret, expr ) \
template< class T, typename X  >               \
using id##_t = decltype( expr );               \
 \
template< typename T, typename X >             \
using has_##id = is_detected_exact< ret, id##_t, T, X >;

#define D_HAS_MEMBER_DETECT_2( id, ret, expr ) \
template< class T, typename X, typename Y >    \
using id##_t = decltype( expr );               \
 \
template< typename T, typename X, typename Y > \
using has_##id = is_detected_exact< ret, id##_t, T, X, Y >;

#endif