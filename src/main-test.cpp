/*	This file is part of id_pool.
	Copyright(C) 2018 by Karl Wesley Hutchinson

	id_pool is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	id_pool is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with id_pool. If not, see <http://www.gnu.org/licenses/>.
*/
#define CATCH_CONFIG_MAIN
#include <catch.hpp>

#include <iostream>
#include <random>

#include "../include/id_pool.hpp"

#define D_PRINT_INTERVALS 0 // TODO: remove
#include "set_flat.hpp"
#include "test.hpp"

#if 0
TEST_CASE( "pool-interval", "[large][core]" )
{
	all_random_test< std::numeric_limits<short>::max(), id_pool_interval<unsigned short> >();
}
#endif

TEST_CASE( "pool-bitset", "[large][core]" )
{
    all_random_test< std::numeric_limits< short >::max( ), id_pool_bitset< 10, 6 > >( );
}
