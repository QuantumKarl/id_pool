/*	This file is part of id_pool.
	Copyright(C) 2018 by Karl Wesley Hutchinson

	id_pool is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	id_pool is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with id_pool. If not, see <http://www.gnu.org/licenses/>.
*/
﻿#if !defined( D_SET_FLAT_HPP )
#define D_SET_FLAT_HPP

#include <vector>
#include <utility>
#include <iterator>
#include <algorithm>
#include <iostream>
using namespace std;

namespace sys_templates
{
	// TEMPLATE STRUCT _Param_tester
	template<class...>
	struct param_tester
	{	// test if parameters are valid
		typedef void type;
	};

	// ALIAS TEMPLATE void_t
	template<class... _Types>	// TRANSITION, needs CWG 1558
	using void_t = typename param_tester<_Types...>::type;

	// TEMPLATE CLASS is_iterator
	template<class T, class = void>
	struct is_iterator
		:
		false_type
	{	// default definition
	};

	template<class T>
	struct is_iterator
	<
		T,
		void_t
		<
			typename iterator_traits<T>::iterator_category
		>
	>
		:
		true_type
	{	// defined if iterator_category is provided by iterator_traits<T>
	};
}

template < typename T, typename compare_t = less<T>, typename equal_t = equal_to<T>, typename allocator_t = allocator<T>  >
class set_flat final
{
public:
	/*---------------- aliases --------------------------------
		these aliases are to mimic stl and to make the
		code shorter / easier to read.
	*/

	// set_flat allocator
	using allocator_type = allocator_t;

	// set_flat container
	using cont_t = vector<T, allocator_type> ;

	// an external version of set_flat's container (with templated allocator)
	template< typename alloc_t >
	using external_cont_t = vector<T, alloc_t>;

	// types
	using value_type		= typename cont_t::value_type;
	using size_type			= typename cont_t::size_type;
	using difference_type	= typename cont_t::difference_type;

	// pointers
	using pointer			= typename cont_t::pointer;
	using const_pointer		= typename cont_t::const_pointer;
	using reference			= typename cont_t::reference;
	using const_reference	= typename cont_t::const_reference;

	// iterators
	using iterator			= typename cont_t::iterator;
	using const_iterator	= typename cont_t::const_iterator;
	using reverse_iterator	= typename cont_t::reverse_iterator;
	using const_reverse_iterator	= typename cont_t::const_reverse_iterator;

	// set_flat class type
	using this_type = set_flat<value_type, compare_t, equal_t, allocator_type>;

	//---------------- constructors --------------------------------

	//--------------------------------
	/* ctor()
		default ctor
	*/
	set_flat() noexcept
		:
		m_v(),
		m_cmp(),
		m_eql()
	{ }
	//---------------- other member operators --------------------------------

	//--------------------------------
	/* operator[]
		subscript non-mutable sequence
	*/
	const_reference operator[]( size_type const i_index ) const
	{
		return m_v[i_index];
	}

	//---------------- member functions --------------------------------

	//--------------------------------
	/* begin()
		return iterator for beginning of non-mutable sequence
	*/
	inline const_iterator begin() const noexcept
	{
		return m_v.cbegin();
	}

	//--------------------------------
	/* back()
		return last element of non mutable sequence
	*/
	const_reference back() const
	{
		return m_v.back();
	}

	//--------------------------------
	/* clear()
		erase all / make empty
	*/
	inline void clear() noexcept
	{
		m_v.clear();
	}

	//--------------------------------
	/* empty()
		ask if sequence is empty
	*/
	inline bool empty() const noexcept
	{
		return m_v.empty();
	}

	//--------------------------------
	/* end()
		return iterator for end of non mutable sequence
	*/
	inline const_iterator end() const noexcept
	{
		return m_v.cend();
	}

	//--------------------------------
	/* erase( value_type const& )
		erase element by value
	*/
	const_iterator erase( value_type const& i_value )
	{
		auto itr = find( i_value );
		return itr != end() ? m_v.erase( itr ) : end();
	}

	//--------------------------------
	/* erase( const_iterator )
		erase element at iterator
	*/
	inline const_iterator erase( const_iterator i_itr )
	{
		return m_v.erase( i_itr );
	}

	//--------------------------------
	/* find( value_type const& )
		find value
	*/
	const_iterator find( value_type const& i_val ) const
	{
		auto itr = lower_bound( begin(), end(), i_val, m_cmp );
		return itr == end() || m_cmp( i_val, *itr ) ? end() : itr;
	}

	//--------------------------------
	/* front()
		return first element of non-mutable sequence
	*/
	const_reference front() const
	{
		return m_v.front();
	}

	//--------------------------------
	/* insert( value_type& i_val )
		insert an element (in the correct sorted position)
	*/
	const_iterator insert( value_type const& i_val )
	{
		auto itr = lower_bound( begin(), end(), i_val, m_cmp );
		if( itr == end() || m_cmp( i_val, *itr ) )
		{
			m_v.insert( itr, i_val );
		}
		return itr;
	}

	//--------------------------------
	/* insert( value_type&& i_val )
		insert an element (in the correct sorted position)
	*/
	const_iterator insert( value_type&& i_val )
	{
		auto itr = lower_bound( cbegin(), cend(), i_val, m_cmp );
		if( itr == cend() || m_cmp( i_val, *itr ) )
		{
			m_v.insert( itr, move( i_val ) );
		}
		return itr;
	}

	//--------------------------------
	/* pop_back()
		remove element at end
	*/
	void pop_back()
	{
		m_v.pop_back();
	}

	//--------------------------------
	/* reserve( i_count )
		set new minimum length of allocated storage
	*/
	inline void reserve( size_type const i_count )
	{
		m_v.reserve( i_count );
	}

	//--------------------------------
	/* size()
		return length of sequence /
		number of inserted items
	*/
	inline size_type size() const noexcept
	{
		return m_v.size();
	}

	//--------------------------------
	/* validate()
		if you have manually manipulated the container via
		a function such as _push_back, then calling this function
		will restore the set properties.

		NOTE: even though its big O notion looks horrid,
		its actually fast since both calls don't have much work to do.
	*/
	inline void validate()
	{
		sort( _begin(), _end(), m_cmp ); // O(n log n)
		auto newEnd = unique( _begin(), _end(), m_eql ); // O(n)
		m_v.resize( distance( m_v.begin(), newEnd ) );
	}

	//---------------- functions that compromise set --------------------------------
	/*
		Using these functions damage the internal properties of:
			being a set
			having all values unique
			sorted data

		Therefore, it can crash if your not careful when you are using these
		functions.

		You can make sure everything is correct by calling validate() after using these functions.
		(preferably once after many calls / batch use these functions then call validate() once)
	*/

	//--------------------------------
	/* _at(size_type const)
		subscript mutable sequence with checking
	*/
	reference _at( size_type const i_pos )
	{
		return m_v.at( i_pos );
	}

	//--------------------------------
	/* _begin()
		return iterator for beginning of mutable sequence
	*/
	inline iterator _begin() noexcept
	{
		return m_v.begin();
	}

	//--------------------------------
	/* _back()
		return last element of mutable sequence
	*/
	reference _back()
	{
		return m_v.back();
	}

	//--------------------------------
	/* _emplace_back( vaList_t&&... )
		insert by moving into element at end
	*/
	template<class... vaList_t>
	void _emplace_back( vaList_t&&... i_values )
	{
		m_v.emplace_back( forward<vaList_t>( i_values )... );
	}

	//--------------------------------
	/* _end()
		return iterator for end of mutable sequence
	*/
	inline iterator _end() noexcept
	{
		return m_v.end();
	}

	//--------------------------------
	/* _find( value_type const& )
		find element by value and return a none cost
		iterator
	*/
	inline iterator _find( value_type const& i_val )
	{
		auto itr = lower_bound( _begin(), _end(), i_val, m_cmp );
		return itr == _end() || m_cmp( i_val, *itr ) ? _end() : itr;
	}

	//--------------------------------
	/* _front()
		return first element of mutable sequence
	*/
	reference _front()
	{
		return m_v.front();
	}

	//--------------------------------
	/* _push_back( value_type&& )
		insert by moving into element at end
	*/
	void _push_back( value_type&& i_value )
	{
		m_v.push_back( move( i_value ) );
	}

	//--------------------------------
	/* _push_back( value_type const& )
		insert by moving into element at end
	*/
	void _push_back( value_type const& i_value )
	{
		m_v.push_back( i_value );
	}

	//--------------------------------
	/* operator[]
		subscript mutable sequence
	*/
	reference operator[]( size_type const i_pos )
	{
		return m_v[i_pos];
	}

private:
	//---------------- private data members ----------------
	cont_t m_v;
	compare_t m_cmp;
	equal_t m_eql;
};

//-------------------------------- ostream operator --------------------------------

//--------------------------------
/* ostream operator <<
	Nicely formats the data to a ostream which is usually the cout ostream.

	For example the code:
		set_flat<size_t> vs{1,2,3,4}; cout << vs << endl;
	prints:
		[1,2,3,4]

	NOTE: remember your data type needs an implementation of ostream operator too
*/
template< typename t, typename c, typename e, typename a>
ostream& operator<< ( ostream& o_stream, set_flat<t, c, e, a> const& i_set_flat )
{
	o_stream << '[';
	auto count = i_set_flat.size();
	if( count > 0 )
	{
		cout << i_set_flat.front();

		if( count > 1 )
		{
			auto cur = i_set_flat.begin();
			auto end = i_set_flat.end();
			++cur; // we have already done the first element (above)
			do
			{
				o_stream << ',' << *cur;
			} while( ++cur != end );
		}
	}
	o_stream << ']' << endl;
	return o_stream;
}

#endif