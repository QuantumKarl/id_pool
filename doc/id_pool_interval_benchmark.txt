C:\Users\QuantumKarl\Documents\repos\id_pool\ide\msvs2015\x64\Release>id_pool_bench.exe
[==========] Running 1 benchmark..
[ RUN      ] IdPoolFixture.Pattern (2 runs, 54 iterations per run)
setup complete
setup complete
[     DONE ] IdPoolFixture.Pattern (242902067.421715 ms)
[   RUNS   ]        Average time: 121451033710.857 us (~29681861.051 us)
                    Fastest time: 121430045465.630 us (-20988245.227 us / -0.017 %)
                    Slowest time: 121472021956.085 us (+20988245.228 us / +0.017 %)
                     Median time: 121451033710.857 us (1st quartile: 121430045465.630 us | 3rd quartile: 121472021956.085 us)

             Average performance: 0.00001 runs/s
                Best performance: 0.00001 runs/s (+0.00000 runs/s / +0.01728 %)
               Worst performance: 0.00001 runs/s (-0.00000 runs/s / -0.01728 %)
              Median performance: 0.00001 runs/s (1st quartile: 0.00001 | 3rd quartile: 0.00001)

[ITERATIONS]        Average time: 2249093216.868 us (~549664.094 us)
                    Fastest time: 2248704545.660 us (-388671.208 us / -0.017 %)
                    Slowest time: 2249481888.076 us (+388671.208 us / +0.017 %)
                     Median time: 2249093216.868 us (1st quartile: 2248704545.660 us | 3rd quartile: 2249481888.076 us)

             Average performance: 0.00044 iterations/s
                Best performance: 0.00044 iterations/s (+0.00000 iterations/s /+0.01728 %)
               Worst performance: 0.00044 iterations/s (-0.00000 iterations/s /-0.01728 %)
              Median performance: 0.00044 iterations/s (1st quartile: 0.00044 |3rd quartile: 0.00044)
[==========] Ran 1 benchmark..
