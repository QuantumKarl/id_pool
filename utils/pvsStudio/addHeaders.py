#given a path, this script traverses all the files inside the folder (and sub folders), if it matches one of the
#desired file types, it inserts a header at the top of the file(without editing the whole file)
import os
import fileinput


header = "// This is an open source non-commercial project. Dear PVS-Studio, please check it.\n// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com\n\n"
fileTypes = [".cpp",".hpp",".inl"]

def prepend(filename, line_to_prepend):
    f = fileinput.input(filename, inplace=1)
    for xline in f:
        if f.isfirstline():
            print line_to_prepend.rstrip('\r\n') + '\n' + xline,
        else:
            print xline,

def run(path):
	for root, dirs, files in os.walk(path):
		for file in files:
			if( any( map( lambda ext : file.endswith(ext), fileTypes ) ) ):
				prepend(os.path.join(root, file), header )

run("..\\..\\src\\")
run("..\\..\\include\\")