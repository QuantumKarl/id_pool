#given a path, this script traverses all the files inside the folder (and sub folders), if it matches one of the
#desired file types, it inserts a header at the top of the file(without editing the whole file)
import os
import fileinput

header = """/*	This file is part of id_pool.
	Copyright(C) 2018 by Karl Wesley Hutchinson

	id_pool is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	id_pool is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with id_pool. If not, see <http://www.gnu.org/licenses/>.
*/"""
fileTypes = [".cpp",".hpp",".inl"]

def prepend(filename, line_to_prepend):
    f = fileinput.input(filename, inplace=1)
    for xline in f:
        if f.isfirstline():
            print line_to_prepend.rstrip('\r\n') + '\n' + xline,
        else:
            print xline,

def run(path):
	for root, dirs, files in os.walk(path):
		for file in files:
			if( any( map( lambda ext : file.endswith(ext), fileTypes ) ) ):
				prepend(os.path.join(root, file), header )

run("..\\..\\src\\")
run("..\\..\\include\\")