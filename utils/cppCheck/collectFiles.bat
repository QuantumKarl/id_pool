rem cppCheck does not read header files...
rem this bat files copies all src to a folder and
rem renames them for cppCheck to inspect

if not exist "%~dp0\files" mkdir %~dp0\files

rem delete the old files
pushd %~dp0\files
	del *.cpp /F /Q
popd

rem in the src dir, copy all files and subfolder's files into a folder here called files
pushd %~dp0..\..\src
   for /r %%a in (*.hpp,*.cpp,*.inl) do (
       copy "%%a" "%~dp0\files\%%~nxa"
   )
popd

rem in the src dir, copy all files and subfolder's files into a folder here called files
pushd %~dp0..\..\include
   for /r %%a in (*.hpp,*.cpp,*.inl) do (
       copy "%%a" "%~dp0\files\%%~nxa"
   )
popd

rem rename all hpps to *.cpp
pushd %~dp0\files
	rename *.hpp *hpp.cpp
popd

rem rename all inl to *.cpp
pushd %~dp0\files
	rename *.inl *inl.cpp
popd